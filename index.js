const path = require('path');
const fs = require('fs');
const { parse } = require('csv-parse/sync');
const { stringify } = require('csv-stringify/sync');
const { getAuditEventsGroupedByModel, convertAuditElementToObject } = require('./utils.js');
const { createTitle, createDescription } = require('./template.js');

const inputCsv = './table.csv';
const outputCsv = './issues.csv';

const csvData = parse(fs.readFileSync(path.join(__dirname, inputCsv), 'UTF-8'));

const auditEvents = csvData.slice(1).map(convertAuditElementToObject);

const auditEventsGroupedByModel = getAuditEventsGroupedByModel(auditEvents);

const issues = Object.entries(auditEventsGroupedByModel).map(([model, events]) => ({
  title: createTitle(model),
  description: createDescription(model, events),
}));

let csvText = 'Title, Description\n';
csvText += stringify(issues);
csvText = csvText.replace(/,\|/g, '|'); // The stringify method adds commas after new lines in the table.

fs.writeFileSync(path.join(__dirname, outputCsv), csvText, 'UTF-8');

console.log(`Done! File created at ${outputCsv}`);
