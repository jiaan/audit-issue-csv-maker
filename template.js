const createTitle = (model) => `Add event type information for audit events using Auditor.audit in \`${model}\``

const createDescription = (model, events) => `
### Problem to solve

Some audit events are named \`audit_operation\`. This isn't a descriptive name and makes it difficult to differentiated from other events.

Additional context is available in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/8497) and in the parent [epic](https://gitlab.com/groups/gitlab-org/-/epics/8571).

### Proposal

Add a meaningful \`name\` for the following events and actions that can be used to differentiate them from other events:

| Event | Action | Location | 
| ------ | ------ | ------ |
${events.map(({ event, action, url }) => {
  return `| \`${event}\` | \`${action || 'unknown'}\` | [file](${url}) |
`
})}

Naming should follow the \`noun_verb\` format and use the past tense. For example \`ci_variable_created\`.

For events created with \`audit_changes\` we can pass the event name using the \`event_type\` argument.

\`\`\`rb
audit_changes(:email, as: 'email address', event_type: 'email_address_added')
\`\`\`

Consider [this MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/92074) as an example of how to do this.

### Implementation plan

1. Add an meaningful event \`name\` for the events in the \`${model}\` model listed in the proposal.
   - The name should follow the \`noun_verb\` format.
1. Update the associated specs.

## Notes

- [For instructions on how to setup your development environment.](https://about.gitlab.com/community/contribute/development/#setup-the-development-enviroment)
- For any questions regarding audit events reach out to a member of the [compliance group](https://about.gitlab.com/handbook/engineering/development/dev/manage/compliance/#group-members).

/label ~backend ~"type::feature" ~"group::compliance" ~"devops::manage" ~"section::dev" ~"Category::Audit Events" ~"Seeking community contributions" ~"Accepting merge requests" ~"workflow::ready for development" ~"priority::3" ~"Category:Audit Events" ~"GitLab Ultimate"
/weight ${Math.ceil(events.length / 2).toString().replace('4', '3')}
/epic https://gitlab.com/groups/gitlab-org/-/epics/8571"
`

module.exports = { createTitle, createDescription };
