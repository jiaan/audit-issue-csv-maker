# Audit issue CSV maker

Input: CSV with list of audit events from this [sheet](https://docs.google.com/spreadsheets/d/119Dvp2ARsoCo8zVvpoLHdcdfygEVW9kCvbRqyh8DPIg/edit#gid=965273176).

Output: CSV with list of audit event issues grouped by model that can be imported into GitLab.

## How to use

1. Download the list of audit events and save as `table.csv`.
1. Install dependencies `yarn install`.
1. Update issue templates in `template.js` as needed.
1. Start the CSV maker `yarn start`.

## System requirements

This project relies on node and yarn. You can install these by running `asdf install`.
