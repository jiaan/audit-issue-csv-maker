const getAuditEventsGroupedByModel = (auditEvents) => {
  const eventsByModel = {};

  auditEvents.forEach((auditEvent) => {
    if (!Object.keys(eventsByModel).includes(auditEvent.model)) {
      eventsByModel[auditEvent.model] = [auditEvent];
    } else {
      eventsByModel[auditEvent.model].push(auditEvent);
    }
  });

  return eventsByModel;
}
  
const convertAuditElementToObject = ([ event, model, action, _, __, url ]) => ({
  event,
  model,
  action,
  url
});

module.exports = { getAuditEventsGroupedByModel, convertAuditElementToObject };
